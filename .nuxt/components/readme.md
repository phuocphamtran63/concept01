# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<HomePageBanner>` | `<home-page-banner>` (components/HomePage/Banner.vue)
- `<HomePageMakeUs>` | `<home-page-make-us>` (components/HomePage/MakeUs.vue)
- `<HomePageServices>` | `<home-page-services>` (components/HomePage/Services.vue)
- `<HomePageSolution>` | `<home-page-solution>` (components/HomePage/Solution.vue)
- `<HomePageWeDid>` | `<home-page-we-did>` (components/HomePage/WeDid.vue)
- `<HomePageWhoAreWe>` | `<home-page-who-are-we>` (components/HomePage/WhoAreWe.vue)
- `<CommonTemplate>` | `<common-template>` (components/common/CommonTemplate.vue)
- `<CommonFooter>` | `<common-footer>` (components/common/Footer.vue)
- `<CommonHeader>` | `<common-header>` (components/common/Header.vue)
