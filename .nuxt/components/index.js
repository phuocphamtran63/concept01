export { default as HomePageBanner } from '../../components/HomePage/Banner.vue'
export { default as HomePageMakeUs } from '../../components/HomePage/MakeUs.vue'
export { default as HomePageServices } from '../../components/HomePage/Services.vue'
export { default as HomePageSolution } from '../../components/HomePage/Solution.vue'
export { default as HomePageWeDid } from '../../components/HomePage/WeDid.vue'
export { default as HomePageWhoAreWe } from '../../components/HomePage/WhoAreWe.vue'
export { default as CommonTemplate } from '../../components/common/CommonTemplate.vue'
export { default as CommonFooter } from '../../components/common/Footer.vue'
export { default as CommonHeader } from '../../components/common/Header.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
